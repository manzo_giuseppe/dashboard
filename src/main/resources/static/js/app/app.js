angular.module('dashboard',['ui.router'])
    .config(function($stateProvider){
          var helloState = {
            name: 'home',
            url: '/',
            template: '<h3>Home</h3>'
          }

          var readingsState = {
            name: 'readings',
            url: '/readings',
            template: '<h3>Readings</h3>'
          }

          var settingsState = {
               name: 'settings',
               url: '/settings',
               template: '<h3>Settings</h3>'
          }

          var aboutState = {
            name: 'about',
            url: '/about',
            template: '<h3>About</h3>'
          }

          $stateProvider.state(helloState);
          $stateProvider.state(readingsState);
          $stateProvider.state(settingsState);
          $stateProvider.state(aboutState);
    })
    .run(function($state){
        $state.go('home');
    })
;